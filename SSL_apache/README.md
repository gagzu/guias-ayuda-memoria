# proteger Apache con Let's Encrypt en Ubuntu 20.04

&nbsp;&nbsp;&nbsp;&nbsp;Let’s Encrypt es una entidad de certificación (CA) que facilita la obtención y la instalación de certificados TLS/SSL gratuitos. 

### Requisitos previos

- Un servidor de Ubuntu 20.04 configurado con una pila LAMP (Linux, Apache, MySql y PHP) el cual explico como hacerlo [aquí](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/pila_LAMP)

- Un nombre de dominio registrado y correctamente configurado los DNS que apunten al servidor al que le aguregaremos el SSL

- Tener habilitado HTTPS a través del firewall (UFW)


**Paso 1: Instalar Certbot**

&nbsp;&nbsp;&nbsp;&nbsp;Instalaremos Certbot para obtener un certificado SSL gratuito para Apache en Ubuntu 20.04 y nos asegurarnos de que esté configurado para renovarse de forma automática.

~~~
sudo apt install certbot python3-certbot-apache
~~~

**Paso 2: Obtener un certificado SSL**

&nbsp;&nbsp;&nbsp;&nbsp;El complemento de Cerbot de Apache se encargará de reconfigurar Apache y volver a cargar la configuración siempre que sea necesario. Para utilizar este complemento, escriba lo siguiente:

~~~
sudo certbot --apache
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Esta secuencia de comandos le solicitará que responda a una serie de preguntas para configurar su certificado SSL. Primero, le solicitará una dirección de correo electrónico válida. Esta dirección se utilizará para las notificaciones de renovación y los avisos de seguridad:

~~~
Output
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator apache, Installer apache
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel): you@your_domain 
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Después de proporcionar una dirección de correo electrónico válida, presione ENTER para continuar con el siguiente paso. Luego, se le solicitará que confirme si acepta las condiciones de servicio de Let’s Encrypt. Puede confirmar pulsando A y, luego, ENTER:

~~~
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v02.api.letsencrypt.org/directory
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(A)gree/(C)ancel: A
~~~

&nbsp;&nbsp;&nbsp;&nbsp;A continuación, se le solicitará que confirme si desea compartir su dirección de correo electrónico con Electronic Frontier Foundation para recibir noticias y otra información. Si no desea suscribirse a su contenido, escriba N. De lo contrario, escriba Y. Luego, presione ENTER para continuar con el siguiente paso.

~~~
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about our work
encrypting the web, EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: N
~~~

&nbsp;&nbsp;&nbsp;&nbsp;En el siguiente paso, se le solicitará que informe a Certbot los dominios para los que desea activar HTTPS. Los nombres de dominio enumerados se obtienen de forma automática de la configuración del host virtual de Apache, por lo tanto, es importante que se asegure de que los ajustes de ServerName y ServerAlias estén configurados correctamente en su host virtual. Si desea habilitar HTTPS para todos los nombres de dominio enumerados (recomendado), puede dejar la solicitud en blanco y presionar ENTER para continuar. De lo contrario, seleccione los dominios para los que desea habilitar HTTPS enumerando cada número correspondiente, separado por comas o espacios, y, luego, presionando ENTER.

~~~
Which names would you like to activate HTTPS for?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: your_domain
2: www.your_domain
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate numbers separated by commas and/or spaces, or leave input
blank to select all options shown (Enter 'c' to cancel):
~~~

&nbsp;&nbsp;&nbsp;&nbsp;A continuación, se le solicitará que seleccione si desea que el tráfico de HTTP se redirija a HTTPS o no. Seleccione 2 para habilitar el redireccionamiento o 1 si desea mantener HTTP y HTTPS como métodos de acceso al sitio web separados.

**Paso 3: Verificar la renovación automática de Certbot**

&nbsp;&nbsp;&nbsp;&nbsp;El paquete certbot que instalamos se encarga de las renovaciones al incluir una secuencia de comandos en /etc/cron.d, que gestiona un servicio systemctl denominado certbot.timer. Esta secuencia de comandos se ejecuta dos veces al día y renovará de forma automática cualquier certificado que caduque en treinta o menos días.

&nbsp;&nbsp;&nbsp;&nbsp;Para verificar el estado de este servicio y asegurarse de que esté activo y en ejecución, puede utilizar lo siguiente:

~~~
sudo systemctl status certbot.timer
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Verá un resultado similar a este:

~~~
Output
● certbot.timer - Run certbot twice daily
     Loaded: loaded (/lib/systemd/system/certbot.timer; enabled; vendor preset: enabled)
     Active: active (waiting) since Tue 2020-04-28 17:57:48 UTC; 17h ago
    Trigger: Wed 2020-04-29 23:50:31 UTC; 12h left
   Triggers: ● certbot.service

Apr 28 17:57:48 fine-turtle systemd[1]: Started Run certbot twice daily.
~~~

**Con esto ya tenemos activo SSL en nuestro servidor**

### Links de referencia

- Para ver la versión completa de está guía lo puede conseguir en su artículo original [aquí](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-20-04-es)

**Feliz codificación** ✌💻