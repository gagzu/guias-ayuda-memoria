# Pasos para configurar un servidor con la pila LAMP (Linux, Apache, MySql, PHP)

La siguiente guía se siguió utilizando droptles de DigitalOcean. Se asume que se creo un droptles y se agrego su respectiva clave SSH

1. Conectarse a su servidor

~~~
ssh -i ~/.ssh/tu_clave_ssh root@server_domain_or_IP
~~~

- instalar Apache
  
~~~
sudo apt update
sudo apt upgrade
sudo apt install apache2
~~~

2. Actualizar el firewall para permitir HTTP y HTTPS. Asegurarse también que tenemos permitido el acceso SSH
    - para verificar el estado del firewall:
    ~~~
    sudo ufw app list
    ~~~
    
    - Para permitir HTTP(S):
    ~~~
    sudo ufw allow "Apache Full"
    ~~~
    
    **Apache Full:** este perfil abre los puertos 80 (tráfico web normal no cifrado) y 443 (tráfico TLS/SSL cifrado).

    2.1 Si estamos en un servidor recién creado (en mi caso uso droplets de DigitalOcean) lo recomendable es crear un usuario diferente de root (lo veremos más adelante) ahora necesitamos asegurarnos de que el firewall permita conexiones SSH para que podamos volver a iniciar sesión la próxima vez. Podemos permitir estas conexiones escribiendo:
    ~~~
    ufw allow OpenSSH
    ~~~
    y luego activamos el fireware
    ~~~
    ufw enable
    ~~~
    
3. Creamos un usuario nuevo con el siguiente comando:
    
    ~~~
    adduser nombre_del_usuario
    ~~~
    
    esto lo que hace es crearnos un usuario y crear su propia carpeta en `/home/nombre_del_usuario` después debemos agregarlo a grupo sudo para que pueda ejecutar comando con permisos de root
    ~~~
    usermod -aG sudo nombre_del_usuario
    ~~~
    
    En la cuenta root en el directorio `~/.ssh/authorized_keys` se encuentra la clave pública local para poder acceder mediante SSH podemos copiar esa estructura de archivo y directorio a nuestra nueva cuenta de usuario en nuestra sesión existente
    ~~~
    rsync --archive --chown=nombre_del_usuario:nombre_del_usuario ~/.ssh /home/nombre_del_usuario
    ~~~
    
    **NOTA:** Ahora lo recomendado es desconectarse como root y seguir el resto del proceso como un usuario de menor privilegios (el que recién se creó) para evitar errores

4. En este punto ya deberíamos poder acceder mediante el navegor *https://midominio.com*

    Verá la página web predeterminada de Apache para Ubuntu 20.04, que se encuentra allí para fines informativos y de prueba. Debería tener un aspecto similar a este:
    
    ![Página de inicio de Apache](https://gitlab.com/gagzu/guias-ayuda-memoria/-/raw/master/assets/img/index-apache.png)
    
    Si ve esta página, su servidor web estará correctamente instalado y el acceso a él será posible a través de su firewall.
    
5. Instalar MySQL
    ~~~
    sudo apt install mysql-server
    ~~~
    Cuando la instalación se complete, se recomienda ejecutar una secuencia de comandos de seguridad que viene preinstalada en MySQL Con esta secuencia de comandos se eliminarán algunos ajustes predeterminados poco seguros y se bloqueará el acceso a su sistema de base de datos. Inicie la secuencia de comandos interactiva ejecutando lo siguiente:
    ~~~
    sudo mysql_secure_installation
    ~~~
    Se le preguntará si desea configurar el VALIDATE PASSWORD PLUGIN.
    
    **Nota:** La habilitación de esta característica queda a discreción del usuario. Si se habilita, MySQL rechazará con un mensaje de error las contraseñas que no coincidan con los criterios especificados. Dejar la validación desactivada será una opción segura, pero siempre deberá utilizar contraseñas seguras y únicas para credenciales de bases de datos.
    
    Para el resto de las preguntas, presione Y y ENTER en cada mensaje. Con esto, se eliminarán algunos usuarios anónimos y la base de datos de prueba, se deshabilitarán las credenciales de inicio de sesión remoto de root y se cargarán estas nuevas reglas para que MySQL aplique de inmediato los cambios que realizó.
    
6. Instalar PHP y configurarlo para poder gestionar diferentes versiones
    - Además del paquete php, se necesitará php-mysql, un módulo PHP que permite que este se comunique con bases de datos basadas en MySQL. También necesitará libapache2-mod-php para habilitar Apache para gestionar archivos PHP. Los paquetes PHP básicos se instalarán automáticamente como dependencias.
    
    ~~~
    sudo apt install php libapache2-mod-php php-mysql
    ~~~
    
     - Agregamos el repositorio ondrej/php que nos permitirá hacer descargas de las distintas versiones de PHP luego actualizamos los repositorios
    ~~~
    sudo add-apt-repository ppa:ondrej/php
    sudo apt update
    sudo apt upgrade
    ~~~
    - A continuación instalamos los paquetes que nos permitira gestionar múltiples versiones de PHP
    ~~~
    sudo apt install software-properties-common libapache2-mod-fcgid
    ~~~
    - Debemos habilitar varios módulos para que el servicio de Apache2 pueda funcionar con varias versiones de PHP:
    ~~~
    sudo a2enmod actions fcgid alias proxy_fcgi
    sudo systemctl restart apache2
    ~~~
        
    - **actions** se utiliza para ejecutar secuencias de comandos de CGI en función del tipo de medio o el método de solicitud.
    - **fcgid** es una alternativa de alto rendimiento de mod_cgi que inicia una suficientes instancias del programa de CGI para gestionar solicitudes simultáneas.
    - **alias** proporciona la asignación de las distintas partes del sistema de archivos del host en el árbol de documentos y la redirección de URL.
    - **proxy_fcgi** le permite a Apache reenviar solicitudes a PHP-FPM.
   
    Hasta este punto ya tenemos php pero para poder probar cambiar entre varias versiones necesitamos servir un index.php de prueba para ver la versión que estamos ejecutando, lo haremos en el paso 6 y luego en el 7 continuaremos con la gestión de multiples versiones de php
---
7. Crear un virtual host para servir la web
    - En la ruta de instalación de Apache (`/etc/apache2/site-available/`) tenemos un fichero de configuración por defecto (no siempre funciona bien) la configuración básica que siempre me funciona el la siguiente:
    
    ~~~
    <VirtualHost *:80>
        ServerName midominio.com
        ServerAlias www.midominio.com
        ServerAdmin example@gmail.com
        DocumentRoot /home/username/public_html

        <Directory /home/username/public_html/>
            Options -Indexes +FollowSymLinks
            AllowOverride None
            Require all granted
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/midominio.com_error.log
        CustomLog ${APACHE_LOG_DIR}/midominio.com_access.log combined
    </VirtualHost>
    ~~~
    
    guardamos eso en un archivo (para el ejemplo lo llamaremos midominio.conf) Ahora tenemos que activar el virtualhost y recargar apache para que se reflejen los cambios con:
    
    ~~~
    sudo a2ensite midonio.conf
    sudo systemctl reload apache2
    ~~~
    
    Creamos un fichero `index.php` dentro de la ruta que apunta nuestro virtualhost (`/home/user/public_html`) y agregamos `<?php phpinfo(); ?>` para verificar que todo quedo configurado correctamente y poder ver la versión de php que tenemos instalada

8. Continuación de la gestión de Múltiples versiones

    - Anteriormente habíamos instalado la última versión de PHP ahora instalaremos una versión inferior para probar cambiar entre diferentes versiones (al momento de escribir esto tenia la versión 7.4 así que instalaré la 7.3)
    ~~~
    sudo apt install php7.3 php7.3-fpm
    ~~~
    - Un paso opcional que podemos hacer es migrar las librerías de una versión a otra en caso de que tengamos corriendo alguna aplicación que dependa de ciertas librerias que no se instalen por defecto en la nueva versión que vamos a cambiar. Eso lo podemos hacer con el siguiente comando:
    ~~~
    sudo apt install $(apt list --installed | grep php7.4- | cut -d'/' -f1 | sed -e 's/7.4/7.3/g')
    ~~~
    
    - Ya que tenemos instalada la nueva versión debemos agregar una configuración extra a nuestro virtual host para poder cambiar de la 7.4 a la 7.3. El siguiente fragmento lo podemos agregar debajo de las etiquetas `<Directory /path/> ... </Directory>`
    
    ~~~
    <FilesMatch \.php$>
        SetHandler "proxy:unix:/var/run/php/php7.3-fpm.sock|fcgi://localhost/"
    </FilesMatch>
    ~~~
    
9. De forma opcional podemos probar si PHP puede establecer conexión con MySQL 
    - Crearemos una base de datos denominada **example_db** y un usuario llamado **example_user**. Primero, establezca conexión con la consola de MySQL usando la cuenta **root**:

    ~~~
    sudo mysql
    CREATE DATABASE example_db;
    ~~~
    
    - Ahora creamos el usuario que utiliza mysql_native_password como método de autenticación predeterminado. 
    ~~~
    CREATE USER 'example_user'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
    ~~~
    
    - Ahora, debemos darle permiso a este usuario a la base de datos example_db:
    ~~~
    GRANT ALL ON example_db.* TO 'example_user'@'%';
    ~~~
    
    - Esto proporcionará al usuario example_user privilegios completos sobre la base de datos example_db y, al mismo tiempo, evitará que este usuario cree o modifique otras bases de datos en su servidor.
    
    - Puede verificar si el usuario nuevo tiene los permisos adecuados al volver a iniciar sesión en la consola de MySQL, esta vez, con las credenciales de usuario personalizadas:
    ~~~
    mysql -u example_user -p
    ~~~
    
    - Observe el indicador -p en este comando, que le solicitará la contraseña que utilizó cuando creó el usuario example_user. Después de iniciar sesión en la consola de MySQL, confirme que tenga acceso a la base de datos example_db:
    ~~~
    HOW DATABASES;
    ~~~
    
    - Con esto se generará el siguiente resultado:
    ~~~
    +--------------------+
    | Database           |
    +--------------------+
    | example_db         |
    | information_schema |
    +--------------------+
    2 rows in set (0.000 sec)
    ~~~
    
    - A continuación, crearemos una tabla de prueba denominada todo_list: Desde la consola de MySQL, ejecute la siguiente instrucción:
    ~~~
    CREATE TABLE example_db.todo_list (
        item_id INT AUTO_INCREMENT,
        content VARCHAR(255),
        PRIMARY KEY(item_id)
    );
    ~~~
    
    - Inserte algunas filas de contenido en la tabla de prueba. Es posible que quiera repetir el siguiente comando algunas veces, usando valores diferentes:
    ~~~
    INSERT INTO example_db.todo_list (content) VALUES ('Mi primer registro');
    ~~~
    
    - Para confirmar que los datos se guardaron correctamente en su tabla, ejecute lo siguiente:
    ~~~
    SELECT * FROM example_db.todo_list;
    ~~~
    - Después de confirmar que haya datos válidos en su tabla de prueba, puede cerrar la consola de MySQL:
    ~~~
    exit
    ~~~
    
    - Bien ahora vamos a copiar el siguiente escript PHP para probar la conexión, creamos en la raíz del virtualhost `todo_list.php` y agregamos el siguiente código:
    ~~~
    <?php
    $user = "example_user";
    $password = "password";
    $database = "example_db";
    $table = "todo_list";

    try {
      $db = new PDO("mysql:host=localhost;dbname=$database", $user, $password);
      echo "<h2>TODO</h2><ol>";
      foreach($db->query("SELECT content FROM $table") as $row) {
        echo "<li>" . $row['content'] . "</li>";
      }
      echo "</ol>";
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    ~~~
    
### Links de referencia
- [Cómo instalar la pila Linux, Apache, MySQL y PHP (LAMP) en Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-es)

- [proteger Apache con Let's Encrypt en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/SSL_apache)

- [Cómo ejecutar varias versiones de PHP en un servidor usando Apache y PHP-FPM en Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-run-multiple-php-versions-on-one-server-using-apache-and-php-fpm-on-ubuntu-18-04-es)

- [Instalar y proteger phpMyAdmin en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/phpmyadmin)

**Feliz codificación** ✌💻