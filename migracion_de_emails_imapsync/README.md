# Como importar cuentas de correo de un servidor a otro usando Imapsync

## Requisitos previos

- Tener un FQDN: ambos deben ser iguales en el servidor A y B. En mi caso, en Cpanel y DigitalOcean

- Servicio IMAP en funcionamiento en ambos servidores

- Si está en alojamiento compartido, asegúrese de conocer la dirección IP del servidor que aloja su contenido web

- Debe conocer los nombres de usuario y las contraseñas de las cuentas de correo electrónico de origen y de destino.

### Paso 1: Instalación de imapsync

Antes de usar Imapsync, definitivamente necesitará instalar primero. EN mi caso estoy en ubuntu

- Primero instale los paquetes de dependencia requeridos (Este comando instala paquetes estándar de Ubuntu):

~~~
sudo apt-get install  \
libauthen-ntlm-perl     \
libclass-load-perl      \
libcrypt-ssleay-perl    \
libdata-uniqid-perl     \
libdigest-hmac-perl     \
libdist-checkconflicts-perl \
libencode-imaputf7-perl     \
libfile-copy-recursive-perl \
libfile-tail-perl       \
libio-compress-perl     \
libio-socket-inet6-perl \
libio-socket-ssl-perl   \
libio-tee-perl          \
libmail-imapclient-perl \
libmodule-scandeps-perl \
libnet-dbus-perl        \
libnet-ssleay-perl      \
libpar-packer-perl      \
libreadonly-perl        \
libregexp-common-perl   \
libsys-meminfo-perl     \
libterm-readkey-perl    \
libtest-fatal-perl      \
libtest-mock-guard-perl \
libtest-mockobject-perl \
libtest-pod-perl        \
libtest-requires-perl   \
libtest-simple-perl     \
libunicode-string-perl  \
liburi-perl             \
libtest-nowarnings-perl \
libtest-deep-perl       \
libtest-warn-perl       \
make                    \
cpanminus
~~~

- Luego instale manualmente el módulo perl `Data::UniqidyMail::IMAPClient`

~~~
cpanm Data::Uniqid Mail::IMAPClient
~~~

- Ahora descargue imapsync e instálelo.

> **NOTA: Consulte las últimas novedades [aquí](https://github.com/imapsync/imapsync/releases)**

~~~
cd /home/<username>/
wget https://github.com/imapsync/imapsync/archive/imapsync-1.xxx.tar.gz
tar zxvf imapsync-1.xxx.tar.gz
~~~

renombramos el directorio imapsync-1.xxx -> imapsync (este paso es opcional, es por comodidad de escribir el nombre del directorio) luego ingresamos a dicho directorio

~~~
mv imapsync-imapsync-1.xxx imapsync
cd imapsync
~~~

- Ahora el comando de instalación (necesita privilegios de root de nuevo): 

~~~
cp imapsync /usr/bin/
~~~


### Paso 2: Verificar que imapsync funciona bien por sí solo

- Una prueba de dependencias que muestra también el ejemplo básico (damos por sentados que estamos dentro del directorio `imapsync` recien creado):

~~~
imapsync
~~~

- Una prueba en vivo que muestra el trabajo de imapsync:

~~~
imapsync --testslive
~~~

- Ahora verifique que el script examples/imapsync_example.sh funcione bien:

~~~
sh examples/imapsync_example.sh
~~~

> Este script hace lo mismo que " imapsync --testslive", pero utiliza explícitamente los 6 parámetros, por lo que será un buen comienzo para su propio script futuro.

### Paso 3: Comenzando con la migración

La migración se debe hacer en partes para asegurar que todo marche correctamente y evitar que rompamos algo, por eso la dividiremos en tres partes.

> Dentro de la carpeta examples encontrarás diferentes ejemplos para hacer una migración de un único mail como también para migraciones masivas, a continuación agrego un par de recomendaciones a seguir que no están en la documentación y que me dieron un dolor de cabeza (si encuentras algún otro que no esté acá por favor no dudes en agregarlo):

> - Si sigues el ejemplo del migración masiva: cuando crees el fichero `mails.txt` donde agregaras el listado de cuentas a migrar, no olvides dejar una o dos líneas en blanco al final del fichero para que pueda funcionar, en caso contrario no siempre funciona y ocasiona errores inesperados

> - Si sigues el ejemplo del migración masiva: en el ejemplo que proporciona imapsync usan un bucle while en bash, dicho bucle toma como entrada un txt con el listados de mails a migrar, no olvides poner la ruta relativa porque sino no lo lee por ejemplo, si el fichero txt con los mails está en la misma carpeta recuerda agregarlo de la siguiente manera => `./mails.txt`


**Parte 1:**
Sugiero comenzar con las banderas --automap --justfolders --dry --no-modulesversion. Si la asignación de carpetas no es buena, agregue algo de --f1f2 fold1=fold2 para solucionarlo.

- **--automap:** adivina la asignación de carpetas, para carpetas como "Enviado", "Correo no deseado", "Borradores", "Todo", "Archivo", "Marcado".

- **--justfolders:** solo hace cosas con las carpetas (ignora los mensajes). Es bueno verificar que la asignación de carpetas sea adecuada para usted.

- **--no-modulesversion:** Evitar que se imprima el listado de módulos con sus respectivas versiones en el log de la migración.

- **--dry:** Hace que imapsync no haga nada de verdad, solo imprime lo que se haría sin --dry

```
echo Looping on account credentials found in mails.txt
echo

{ while IFS=';' read  h1 u1 p1 h2 u2 p2 fake
		do 
				{ echo "$h1" | tr -d '\r' | egrep '^#|^ *$' ; } > /dev/null && continue # this skip commented lines in file.txt
				echo "========= Starting imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				imapsync --host1 "$h1" --user1 "$u1" --password1 "$p1" \
								 --host2 "$h2" --user2 "$u2" --password2 "$p2" \
								 --dry \
								 --automap \
								 --justfolders \
								 --no-modulesversion \
								 "$@"
				echo "========= Ended imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				echo
		done 
} < ./mails.txt
```

**Parte 2:**
Luego elimine --dry y ejecute para crear carpetas en host2.

```
echo Looping on account credentials found in mails.txt
echo

{ while IFS=';' read  h1 u1 p1 h2 u2 p2 fake
		do 
				{ echo "$h1" | tr -d '\r' | egrep '^#|^ *$' ; } > /dev/null && continue # this skip commented lines in file.txt
				echo "========= Starting imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				imapsync --host1 "$h1" --user1 "$u1" --password1 "$p1" \
								 --host2 "$h2" --user2 "$u2" --password2 "$p2" \
								 --dry \ *<== Se elimina esta línea*
								 --automap \
								 --justfolders \
								 --no-modulesversion \
								 "$@"
				echo "========= Ended imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				echo
		done 
} < ./mails.txt
```


**Parte 3:**
Si todo va bien hasta ahora, elimine --justfolders para comenzar a sincronizar los mensajes.

```
echo Looping on account credentials found in mails.txt
echo

{ while IFS=';' read  h1 u1 p1 h2 u2 p2 fake
		do 
				{ echo "$h1" | tr -d '\r' | egrep '^#|^ *$' ; } > /dev/null && continue # this skip commented lines in file.txt
				echo "========= Starting imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				imapsync --host1 "$h1" --user1 "$u1" --password1 "$p1" \
								 --host2 "$h2" --user2 "$u2" --password2 "$p2" \
								 --automap \
								 --justfolders \ *<== Se elimina esta línea*
								 --no-modulesversion \
								 "$@"
				echo "========= Ended imapsync from host1 $h1 user1 $u1 to host2 $h2 user2 $u2 ========="
				echo
		done 
} < ./mails.txt
```

Ya con esto hemos terminado de hacer toda la migración

### Links de referencia

- [Página oficial del script para hacer la migración **imapsync**](https://imapsync.lamiral.info/)

- [Tutorial oficial para hacer la instalación en ubuntu](https://imapsync.lamiral.info/INSTALL.d/INSTALL.Ubuntu.txt)

- [Tutorial oficial para hacer la migración](https://imapsync.lamiral.info/doc/TUTORIAL_Unix.html)

- [Tutorial oficial para hacer una migración masiva](https://imapsync.lamiral.info/FAQ.d/FAQ.Massive.txt)


**Feliz codificación** ✌💻
