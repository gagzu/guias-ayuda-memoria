# README
Este repositorio lo utilizo como ayuda memoria. Es una compilación de diferentes guías de configuraciones e instalaciones, a menudo me pasa (espero que no sea el único al que le pase 😆) que tengo que hacer por poner un ejemplo la configuración de un servidor con la pila LAMP preparado para producción y por ser una tarea que no hago muy a menudo algunos comandos los olvidos o parte del proceso, para evitar eso decidí tener este repo al que puedo revisitar y poder tener una guía documentada y evitar cometer errores y facilitarme el proceso

A ti que estas leyendo esto espero alguna de mis guías te pueda servir 🤓

### Tabla de contenido

- [Cómo instalar la pila Linux, Apache, MySQL y PHP (LAMP) en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/pila_LAMP)

- [proteger Apache con Let's Encrypt en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/SSL_apache)

- [Instalar y proteger phpMyAdmin en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/phpmyadmin)

**Feliz codificación** ✌💻

