# Instalar y proteger phpMyAdmin en Ubuntu 20.04

## Requisitos previos

- Tener una instalación LAMP (Linux, Apache, MySQL y PHP) en su servidor Ubuntu 20.04. Si aún no lo hizo, puede seguir esta [guía para instalar una pila LAMP en Ubuntu 20.04](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/pila_LAMP))

- Tener un dominio existente configurado con un certificado SSL/TLS puede seguir esta [guía para proteger Apache con Let’s Encrypt en Ubuntu 20.04.](https://gitlab.com/gagzu/guias-ayuda-memoria/-/tree/master/SSL_apache)

    Una vez completados estos pasos, estará listo para comenzar con esta guía.


### Paso 1: Instalación de phpMyAdmin
&nbsp;&nbsp;&nbsp;&nbsp;Para comenzar, instalaremos phpMyAdmin desde los repositorios predeterminados de Ubuntu.

~~~
sudo apt install phpmyadmin php-mbstring
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Se le harán algunas preguntas para configurar su instalación de forma correcta.

> Advertencia: Cuando aparece el mensaje, “apache2” se resalta, pero no se selecciona. Si no pulsa ESPACIO para seleccionar Apache, el instalador no moverá los archivos necesarios durante la instalación. Pulse ESPACIO, TAB y luego ENTER para seleccionar Apache.

- Para seleccionar el servidor, elija apache2
![Selelcionar servidor para PhpMyAdmin](https://gitlab.com/gagzu/guias-ayuda-memoria/-/raw/master/assets/img/configuring-phpmyadmin-web-server.jpg)

- Cuando se le pregunte si utiliza dbconfig-common para configurar la base de datos, seleccione Yes.
![Configurar dbconfig-common](https://gitlab.com/gagzu/guias-ayuda-memoria/-/raw/master/assets/img/configuring-phpmyadmin-database.jpg)

- Luego, se le solicitará elegir y confirmar una contraseña para la aplicación de MySQL para phpMyAdmin.
![ingresar contraseña](https://gitlab.com/gagzu/guias-ayuda-memoria/-/raw/master/assets/img/configuring-phpmyadmin-password.jpg)

- Se le pedirá que confirme la contraseña
![Confirmar contraseña](https://gitlab.com/gagzu/guias-ayuda-memoria/-/raw/master/assets/img/configuring-phpmyadmin-confirm-password.jpg)

- Activar el módulo de PHP mbstring

    ~~~
    sudo phpenmod mbstring
    ~~~

- A continuación, reinicie Apache para que sus cambios surtan efecto:

    ~~~
    sudo systemctl reload apache2
    ~~~
    
&nbsp;&nbsp;&nbsp;&nbsp;De esta manera, phpMyAdmin quedará instalado y configurado. Sin embargo, para poder iniciar sesión y comenzar a interactuar con sus bases de datos de MySQL, deberá asegurarse de que sus usuarios de MySQL tengan los privilegios necesarios para interactuar con el programa.

### Paso 2: Crear un usuario administrativo de MySQL

- Comience por iniciar sesión en el servidor MySQL como usuario root:

    ~~~
    sudo mysql
    ~~~
- Desde dentro del shell de MySQL, ejecute los siguientes comandos que crearán un nuevo usuario administrativo y otorgarán los permisos adecuados:

    ~~~
    mysql> CREATE USER 'padmin'@'localhost' IDENTIFIED BY 'password';
    mysql> GRANT ALL PRIVILEGES ON *.* TO 'padmin'@'localhost' WITH GRANT OPTION;
    ~~~
> Nota: recuerda cambiar 'password' por una contraseña segura

### Paso 3:  Accediendo a phpMyAdmin

&nbsp;&nbsp;&nbsp;&nbsp;Para acceder a la interfaz phpMyAdmin, abra su navegador favorito y escriba el nombre de dominio o la dirección IP pública de su servidor seguido de /phpmyadmin:

~~~
https://your_domain_or_ip_address/phpmyadmin
~~~

### Paso 4: Asegurar phpMyAdmin

&nbsp;&nbsp;&nbsp;&nbsp;Para agregar una capa adicional de seguridad, protegeremos con contraseña el directorio phpMyAdmin configurando una autenticación básica.

- Primero crearemos un archivo de contraseña con los usuarios usando la herramienta `htpasswd` que viene con el paquete Apache. Almacenaremos el archivo .htpasswd en el directorio `/etc/phpmyadmin`

~~~
sudo htpasswd -c /etc/phpmyadmin/.htpasswd padmin
~~~

- El comando anterior le pedirá que ingrese y confirme la contraseña del usuario.

> En este ejemplo, estamos creando un usuario llamado padmin. Puede elegir cualquier nombre de usuario, no tiene que ser el mismo que el usuario administrativo de MySQL.

- Si desea agregar un usuario adicional, puede usar el mismo comando sin la -c bandera:

~~~
sudo htpasswd /etc/phpmyadmin/.htpasswd padmin2
~~~

&nbsp;&nbsp;&nbsp;&nbsp;El siguiente paso es configurar Apache para proteger con contraseña el directorio phpMyAdmin y usar el .htpasswdarchivo.

&nbsp;&nbsp;&nbsp;&nbsp;Para hacerlo, abra el `phpmyadmin.conf` archivo que se creó automáticamente durante la instalación de phpMyAdmin:

~~~
sudo nano /etc/apache2/conf-available/phpmyadmin.conf
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Inserte o edite las lineas marcadas:
~~~
<Directory /usr/share/phpmyadmin>
  ...
  Options  +FollowSymLinks +Multiviews +Indexes <- esta línea
  DirectoryIndex index.php

  AllowOverride None <- esta línea
  AuthType basic <- esta línea
  AuthName "Authentication Required" <- esta línea
  AuthUserFile /etc/phpmyadmin/.htpasswd <- esta línea
  Require valid-user <- esta línea

  <IfModule mod_php5.c>
  ...
~~~

&nbsp;&nbsp;&nbsp;&nbsp;Guarde y cierre el archivo y reinicie Apache para que los cambios surtan efecto:

~~~
sudo systemctl reload apache2
~~~

- Ahora, cuando acceda a su phpMyAdmin, se le pedirá que ingrese las credenciales de inicio de sesión del usuario que creó anteriormente

~~~
https://your_domain_or_ip_address/phpmyadmin
~~~

- Después de ingresar la autenticación básica, se lo dirigirá a la página de inicio de sesión de phpMyAdmin, donde debe ingresar sus credenciales de inicio de sesión de usuario administrativo de MySQL.

Con esto ya hemos instalado correctamente y asegurado phpMyAdmin

### Links de referencia

- [Cómo instalar y proteger phpMyAdmin con Apache en Ubuntu 18.04](https://linuxize.com/post/how-to-install-and-secure-phpmyadmin-with-apache-on-ubuntu-18-04/)

- [Cómo instalar y proteger phpMyAdmin en Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/como-instalar-y-proteger-phpmyadmin-en-ubuntu-18-04-es)


**Feliz codificación** ✌💻